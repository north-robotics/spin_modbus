CON
  _clkmode = xtal1 + pll16x      
  _xinfreq = 5_000_000

  CRC16_POLY = $8005
  CRC16_MASK = $8000
  
  CCITT_POLY = $1021
  CCITT_MASK = $8000
                  
  CRC16_CRC = $0000
  MODBUS_CRC = $ffff
  XMODEM_CRC = $0000
  
OBJ
  serial: "serial"

PUB ReverseBits(i, num) | j, k
  j := 0
  k := 0
  repeat while (k < num)
    j :=(j << 1) | (i & 1)
    i >>= 1
    k++

  return j      
  
PUB CalculateCrc16(data, dataLength, crc, polynomial, reverse) | value   
  repeat dataLength
    value := byte[data++]

    if reverse
      value := ReverseBits(value, 8)
      
    crc ^= value << 8

    repeat 8
      if crc & $8000
        crc := crc << 1 ^ polynomial
      else
        crc := crc << 1

  crc &= $ffff

  if reverse
    crc := ReverseBits(crc, 16)
  
  return crc

PUB CalculateLrc(data, inputLength) | lrc
  lrc := 0      
  repeat inputLength        
    lrc += byte[data++]
  lrc := lrc ^ $ff + 1

  return lrc  

PUB Crc16(data, dataLength)
  return CalculateCrc16(data, dataLength, CRC16_CRC, CRC16_POLY, true)

PUB ModbusRtu(data, dataLength)
  return CalculateCrc16(data, dataLength, MODBUS_CRC, CRC16_POLY, true)

PUB XModem(data, dataLength)
  return CalculateCrc16(data, dataLength, XMODEM_CRC, CCITT_POLY, false)

PUB ModbusAscii(data, dataLength)
  return CalculateLrc(data, dataLength)