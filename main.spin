CON
  _clkmode = xtal1 + pll16x      
  _xinfreq = 5_000_000
  
  SERIAL_PORT = 0               ' input serial port
  SERIAL_BAUD = 115200
  SERIAL_RTU_DELAY_CNT = 960_000_000 / SERIAL_BAUD 
  SERIAL_RX = 31
  SERIAL_TX = 30
  
  MODBUS_PORT = 1               ' rs-485 com port number
  MODBUS_BAUD = 38_400
  MODBUS_RTU_DELAY_CNT = 960_000_000 / MODBUS_BAUD ' 1.5chars * 8bits * 1,000,000us/s * 80cnt/us / baudrate
  MODBUS_EN = 16                ' chip enable pin                                         
  MODBUS_TE = 17                ' TX enable pin
  MODBUS_RX = 18                ' RX pin
  MODBUS_TX = 19                ' TX pin

OBJ  
  com: "02-COMn"

VAR
  byte requestBuffer[32]
  byte responseBuffer[32]

PRI WriteBuffer(port, buffer, bufferLen)
  repeat bufferLen
    com.tx(port, byte[buffer++])

PRI RtuRequest(request, requestLen, response) : responseLen | lastByteCnt, data
  com.rxflush(MODBUS_PORT)   ' flush out any garbage in the rtu rx buffer
  outa[MODBUS_EN] := 1       ' enable chip (needed for multi-master) 
  outa[MODBUS_TE] := 1       ' enable transmit
  
  ' send requeest
  WriteBuffer(MODBUS_PORT, request, requestLen)
   
  com.txflush(MODBUS_PORT)            ' wait for transmit to finish
  waitcnt(MODBUS_RTU_DELAY_CNT + cnt) ' wait for a 1.5 char delay
  outa[MODBUS_TE] := 0                ' disable transmit (doing this after the delay avoids garbage data)
  
  responseLen := ReadRtuMessage(MODBUS_PORT, MODBUS_RTU_DELAY_CNT, response)
   
  outa[MODBUS_EN] := 0      ' disable chip

PRI ReadRtuMessage(port, delay, message) : messageLen | lastByteCnt, data 
  byte[message++] := com.rx(port) ' read (and wait for) the first byte of the response
  lastByteCnt := cnt
  messageLen := 1
  ' keep reading response bytes until we hit a delay of 1.5 chars
  repeat until (cnt - lastByteCnt) > delay
    data := com.rxcheck(port)
    ' store the byte if we received one
    if data <> -1
      byte[message++] := data
      lastByteCnt := cnt
      messageLen++       
    
PUB Start | ptr, data, last_byte_time, requestLen, responseLen
  com.init
  com.addport(SERIAL_PORT, SERIAL_RX, SERIAL_TX, SERIAL_BAUD)
  com.addport(MODBUS_PORT, MODBUS_RX, MODBUS_TX, MODBUS_BAUD)  'Port,RX,TX, Baud
  com.start 
  
  dira[MODBUS_TE] := 1 ' set TX enable pin to write
  outa[MODBUS_TE] := 0
  
  dira[MODBUS_EN] := 1
  outa[MODBUS_EN] := 0
  
  repeat
    requestLen := ReadRtuMessage(SERIAL_PORT, SERIAL_RTU_DELAY_CNT, @requestBuffer)
    responseLen := RtuRequest(@requestBuffer, requestLen, @responseBuffer)
    WriteBuffer(SERIAL_PORT, @responseBuffer, responseLen)